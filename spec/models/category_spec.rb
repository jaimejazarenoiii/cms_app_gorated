require 'spec_helper'

describe Product do
  before { @category = Category.new(name: "Shoes", is_active: true) }

  subject { @category }

  it { should respond_to(:name) }
  it { should respond_to(:is_active) }
  it {should be_valid}

  describe "when name is not present" do
    before { @category.name = " " }
    it { should_not be_valid }
  end

  describe "when price is not present" do
    before { @category.price = " " }
    it { should_not be_valid }
  end

  describe "when username is already taken" do
    before do
      prod=@category.dup
      prod.name=@category.name
      prod.save
    end
    it { should_not be_valid }
  end

end
