require 'spec_helper'

describe Product do
  before { @product = Product.new(name: "Example User", price: 23.23) }

  subject { @product }

  it { should respond_to(:name) }
  it { should respond_to(:price) }
  it {should be_valid}

  describe "when name is not present" do
    before { @product.name = " " }
    it { should_not be_valid }
  end

  describe "when price is not present" do
    before { @product.price = " " }
    it { should_not be_valid }
  end

  describe "when username is already taken" do
    before do
      prod=@product.dup
      prod.name=@product.name
      prod.save
    end
    it { should_not be_valid }
  end

end
