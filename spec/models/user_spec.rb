require 'spec_helper'

describe User do
  before { @user = User.new(fname: "Example User",lname: "Example User",username: "Example User",password: "Example User",
  	password_confirmation: "Example User", email: "user@example.com") }

  subject { @user }

  it { should respond_to(:fname) }
  it { should respond_to(:lname) }
  it { should respond_to(:username) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:email) }
  it {should be_valid}

  describe "when first name is not present" do
    before { @user.fname = " " }
    it { should_not be_valid }
  end

  describe "when last name is not present" do
    before { @user.lname = " " }
    it { should_not be_valid }
  end

  describe "when email is not present" do
    before { @user.email = " " }
    it { should_not be_valid }
  end

  describe "when username is not present" do
    before { @user.username = " " }
    it { should_not be_valid }
  end

  describe "when password is not present" do
    before { @user.password = " " }
    it { should be_valid }
  end

  describe "when password confirmation is not present" do
    before { @user.password_confirmation = " " }
    it { should_not be_valid }
  end

  describe "when email is already taken" do
    before do
      user_email=@user.dup
      user_email.email=@user.email.upcase
      user_email.save
    end
    it { should_not be_valid }
  end

  describe "when username is already taken" do
    before do
      user_username=@user.dup
      user_username.username=@user.username.upcase
      user_username.save
    end
    it { should_not be_valid }
  end

  describe "when email format is invalid" do
    it "should be invalid" do
      addresses = %w[user@qq,com user_at_foo.org xv.232@foo.
                     foo@bar_baz.com foo@bar--+baz.com]
      addresses.each do |invalid_address|
        @user.email = invalid_address
        expect(@user).not_to be_valid
      end
    end
  end
end
